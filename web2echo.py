# -*- coding: utf-8 -*-
'''
    web2echo.py: Converts the World English Bible into echobible format.

    Copyright (C) 2014  Tjaart van der Walt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import os
import urllib.request
import zipfile

from lxml import etree

if __name__ == "__main__":
    # Map WEB book names to echobible book names
    # The text includes apocryphal books. This *should* not be a problem for echobible,
    # as it will not get displayed.
    book_mapping = {"GEN": "GE",
                    "EXO": "EX",
                    "LEV": "LE",
                    "NUM": "NU",
                    "DEU": "DE",
                    "JOS": "JOS",
                    "JDG": "JG",
                    "RUT": "RU",
                    "1SA": "1SA",
                    "2SA": "2SA",
                    "1KI": "1KI",
                    "2KI": "2KI",
                    "1CH": "1CH",
                    "2CH": "2CH",
                    "EZR": "EZR",
                    "NEH": "NE",
                    "EST": "ES",
                    "JOB": "JOB",
                    "PSA": "PS",
                    "PRO": "PR",
                    "ECC": "EC",
                    "SNG": "SONG",
                    "ISA": "ISA",
                    "JER": "JER",
                    "LAM": "LA",
                    "EZK": "EZE",
                    "DAN": "DA",
                    "HOS": "HO",
                    "JOL": "JOE",
                    "AMO": "AM",
                    "OBA": "OB",
                    "JON": "JON",
                    "MIC": "MIC",
                    "NAM": "NA",
                    "HAB": "HAB",
                    "ZEP": "ZEP",
                    "HAG": "HAG",
                    "ZEC": "ZEC",
                    "MAL": "MAL",

                    "TOB": "TOB",
                    "JDT": "JDT",
                    "ESG": "ESG",
                    "WIS": "WIS",
                    "SIR": "SIR",
                    "BAR": "BAR",
                    "LJE": "LJE",
                    "S3Y": "S3Y",
                    "SUS": "SUS",
                    "BEL": "BEL",
                    "1MA": "1MA",
                    "2MA": "2MA",
                    "1ES": "1ES",
                    "MAN": "MAN",
                    "PS2": "PS2",
                    "3MA": "3MA",
                    "2ES": "2ES",
                    "4MA": "4MA",
                    
                    "MAT": "MT",
                    "MRK": "MR",
                    "LUK": "LU",
                    "JHN": "JOH",
                    "ACT": "AC",
                    "ROM": "RO",
                    "1CO": "1CO",
                    "2CO": "2CO",
                    "GAL": "GA",
                    "EPH": "EPH",
                    "PHP": "PHP",
                    "COL": "COL",
                    "1TH": "1TH",
                    "2TH": "2TH",
                    "1TI": "1TI",
                    "2TI": "2TI",
                    "TIT": "TIT",
                    "PHM": "PHM",
                    "HEB": "HEB",
                    "JAS": "JAS",
                    "1PE": "1PE",
                    "2PE": "2PE",
                    "1JN": "1JO",
                    "2JN": "2JO",
                    "3JN": "3JO",
                    "JUD": "JUDE",
                    "REV": "RE"}
    
    # First we need to make sure we have downloaded the bible text.
    if(not os.path.isfile('eng-web_usfx.xml')):
        urllib.request.urlretrieve("http://ebible.org/eng-web/eng-web_usfx.zip", "eng-web_usfx.zip")

        # Extract the xml file to the current working dir
        zfile = zipfile.ZipFile("eng-web_usfx.zip")
        zfile.extract("eng-web_usfx.xml")
        
    tree = etree.iterparse('eng-web_usfx.xml', events=("start", "end"))
    # The verse tags is a bit strange. At the start of a verse you have a <v/> tag,
    # and at the end you have a <ve/> tag. Between these tags in_verse is true
    in_verse = False
    text = ""
    for action, elem in tree:
        # Action can be at  the start or end of a tag, we perform our parsing at the start
        if(action == "start"):
            # Get the current book name
            if(elem.tag == "book"):
                book = elem.get("id")
            # Get the current chapter number
            if(elem.tag == "c"):
                chapter = elem.get("id")

            if(in_verse):
                # Here we come to the end of the verse, time to print it.
                if(elem.tag == "ve"):
                    print("%s %s:%s %s" % (mapped_book, chapter, verse, text))
                    in_verse = False
                else:
                    # <f> is a comment tag... for now we ignore these
                    # If we are inside a tag e.g <q>text</q> we want to extract the text
                    if(elem.tag is not "f" and elem.text is not None):
                        text += " " + elem.text.rstrip("\n")
                    if(elem.tail is not None):
                        text += " " + elem.tail.rstrip("\n")

            if(elem.tag == "v"):
                in_verse = True
                verse = elem.get("id")
                mapped_book = book_mapping[book]
                # <v> does not have any text, but can have a tail. This is because
                # start verse and end verse does not have the same tag e.g:
                # <v/>vers text<ve/>
                if(elem.tail is not None):
                    text = elem.tail.rstrip("\n")
