                                README
                                ======

Author: Tjaart van der Walt
Date: 2014-08-18 11:58:04 CDT


Table of Contents
=================
1 Web2echo
    1.1 License
    1.2 Quick and dirty
    1.3 Python 3
    1.4 Building from scratch
    1.5 A word the text
        1.5.1 Accuracy
        1.5.2 Footnotes
        1.5.3 Apocryphal books


1 Web2echo 
===========
This script converts the freely available [World English Bible] 
into a format suitable for use with the  [the Echo Bible Reader].


[World English Bible]: http://www.ebible.org/web/
[the Echo Bible Reader]: https://bitbucket.org/jpolcol/echobible

1.1 License 
------------
The python script is licensed under GNU GPL.
The WEB text is in the Public Domain ([http://www.ebible.org/web/copyright.htm])


1.2 Quick and dirty 
--------------------
If you do not care about building the bible file yourself, follow these steps:

Download the WEB echobible file to your echobible directory. For Arch linux it is `/usr/share/echobible`


  sudo  wget -O /usr/share/echobible/web https://bitbucket.org/tjaartvdwalt/web2echo/raw/bc78467d144c72fc85beb2d7d41c0f6e93aa0378/text/web


To start using your new bible you need to edit the settings file. `~/echobible/settings`
Comment out the line `BIBLE="kjv-pce"` and add a line`BIBLE="web"`:


  #BIBLE="kjv-pce"
  BIBLE="web"


1.3 Python 3 
-------------
It is important to note is that this script only works with Python 3. 
Python 3 made some non backward compatible changes, and I chose to use the newest version.

1.4 Building from scratch 
--------------------------

The first time you run the script it might take a while to complete.
This is because it needs to download the bible source file.

While parsing the xml file the script outputs the results to stdout.
Typically you would want to redirect stdout to some file.


  python web2echo.py > web



Then copy the resulting file to your echobible directory. For Arch linux it is `/usr/share/echobible`


  sudo cp web /usr/share/echobible/


To start using your new bible you need to edit the settings file. `~/echobible/settings`
Comment out the line `BIBLE="kjv-pce"` and add a line`BIBLE="web"`:


  #BIBLE="kjv-pce"
  BIBLE="web"


1.5 A word the text 
--------------------

1.5.1 Accuracy 
~~~~~~~~~~~~~~~
The text *should* be accurate, unless there is a bug with the parsing.
Please let me know if you find anything.

1.5.2 Footnotes 
~~~~~~~~~~~~~~~~
I have chosen to exclude the footnotes at this stage.
I am unsure if echobible has a built in way of dealing with footnotes.

1.5.3 Apocryphal books 
~~~~~~~~~~~~~~~~~~~~~~~
The WEB text includes apocryphal books.
These get parsed along with the rest of the books, but you will not see them when echobible.
